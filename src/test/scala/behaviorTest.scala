package edu.luc.cs.laufer.cs372.shapes

import fixtures._
import Behaviors._
import Structures._
import org.scalatest.FunSuite
import scalamu._
import scalaz.syntax.equal._
import scalaz.std.anyVal._ // for assert_=== to work on Int

class behaviorTest extends FunSuite{


    test("size works") {
      def w(v: Location[Rectangle]): ShapeF[ShapeF[Unit]] = v

      simpleEllipse cata size assert_=== 1
      simpleRectangle cata size assert_=== 1
      simpleLocation cata size assert_=== 1
      basicGroup cata size assert_=== 2
      simpleGroup cata size assert_=== 2
      complexGroup cata size assert_=== 5


    }
    test("bounding box works") {
      def w(v: Location[Rectangle]): ShapeF[ShapeF[Unit]] = v

      w(simpleEllipse cata boundingBox) assert_=== w(Location( -50, -30, Rectangle( 100, 60 ) ) )

      w(simpleRectangle cata boundingBox) assert_=== w(Location( 0, 0,  Rectangle(80,120) ) )
      w(simpleLocation cata boundingBox) assert_=== w(Location( 70, 30,  Rectangle(80,120) ) )
      //The test below is the one that isn't passing
     w(basicGroup cata boundingBox) assert_=== w(Location( -50, -30,  Rectangle(100,70) ) )
      w(simpleGroup cata boundingBox) assert_=== w(Location( 150, 70,  Rectangle(350,280) ) )
      w(complexGroup cata boundingBox) assert_=== w(Location( 30, 60,  Rectangle(470,320) ) )
  }

}

