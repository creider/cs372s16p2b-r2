package edu.luc.cs.laufer.cs372.shapes

import org.scalatest.FunSuite
import Structures._
import scalamu._

class lawTests extends FunSuite {
  import scalaz.syntax.equal._
  import scalaz.std.anyVal._

  test("equality works") {
    (Rectangle(3,3): ShapeF[Unit]) assert_=== (Rectangle(3,3): ShapeF[Unit])
    (Ellipse(3,3): ShapeF[Unit] ) assert_=== (Ellipse(3,3): ShapeF[Unit])
    (Location(0,0,Rectangle(3,3)): ShapeF[ShapeF[Unit]]) assert_=== (Location(0,0, Rectangle(3,3)): ShapeF[ShapeF[Unit]])
    (Group(Rectangle(3,3),Rectangle(1,2),Rectangle(3,3)): ShapeF[ShapeF[Unit]]) assert_=== (Group(Rectangle(3,3),Rectangle(1,2),Rectangle(3,3)): ShapeF[ShapeF[Unit]])

    //    constant(3) assert_=== constant(3)
  //  uminus(constant(3)) assert_=== uminus(constant(3))
  }

}

//Note: speccifying ShapeF[_] was necessary. Can that be changed so ShapeF accepts a non-anonymous type?