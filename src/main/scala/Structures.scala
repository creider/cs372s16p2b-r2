package edu.luc.cs.laufer.cs372.shapes

import scalaz.{ Equal, Functor }
import scalamu._

object Structures {

  sealed trait ShapeF[+A]
  //todo: figure out if Ellipse and Rectangle should extend ShapeF{Nothing] instead
  case class Ellipse(x: Int, y: Int) extends ShapeF[Nothing]

  case class Rectangle(width: Int, height: Int) extends ShapeF[Nothing]

  case class Location[A](x: Int, y: Int, shape: A) extends ShapeF[A]

  case class Group[A](ShapeF: A*) extends ShapeF[A]


  // implicit value (scalaz)

  implicit object ShapeFFunctor extends Functor[ShapeF] {
    //Note: recursion in Location and Group
    def map[A, B](fa: ShapeF[A])(f: A => B): ShapeF[B] = fa match {
      case Ellipse(x, y) => Ellipse(x, y)
      case Rectangle(x, y) => Rectangle(x, y)
      case Location(x, y, shape) => Location(x, y, f(shape))
      case Group(shapes@_*) => Group(shapes.map(f): _*)
    }

  }
  // Equality
  //Not sure if correct

  implicit def ShapeFEqual[A](implicit A: Equal[A]): Equal[ShapeF[A]] = Equal.equal {
    case (Rectangle(x, y), Rectangle(a,b) ) => (x == a) && (y == b)
    case (Ellipse(x, y), Ellipse(a,b) ) => (x == a) && (y == b)
    case (Location(x,y,z), Location(a,b,c) ) => (x == a) && (y == b) && A.equal(z,c)
    case (Group(s @ _*), Group(t @ _*)) => scalaz.std.list.listEqual(A).equal(s.toList, t.toList)

    case _ => false
  }
  //Fixpoint

  type Shape = µ[ShapeF]

  // factory methods for each shape

  object ShapeFactory {

    def ellipse(x: Int, y: Int) = In[ShapeF](Ellipse(x, y))

    def rectangle(x: Int, y: Int) = In[ShapeF](Rectangle(x, y))

    def location(x: Int, y: Int, shape: Shape) = In[ShapeF](Location(x, y, shape))

    def group(shapes: Shape*): Shape = In[ShapeF](Group(shapes: _*))

  }

}