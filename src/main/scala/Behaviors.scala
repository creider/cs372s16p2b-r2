package edu.luc.cs.laufer.cs372.shapes

import edu.luc.cs.laufer.cs372.shapes.Structures._

import scalamu._

object Behaviors {
  // @tparam A
  val boundingBox: Algebra[ShapeF, Location[Rectangle]] = {

    case Rectangle(a, b) => Location(0, 0, Rectangle(a, b))
    case Ellipse(a,b) => Location(-a,-b, Rectangle(2*a,2*b))
    case Location(x: Int, y: Int, s:ShapeF[_]) => {
      Location(s.x + x, s.y + y, s.shape)
    }
    case Group(shapes@_*) =>
      //these both correspond to x coords
      val xMin = shapes.map((s) => s.x).min
      val yMin = shapes.map((s) => s.y).min
      val xMax = shapes.map((s)=> s.x + s.shape.width).max
      val yMax = shapes.map((s)=> s.y + s.shape.height).max
      val width = xMax - xMin
      val height = yMax - yMin
      Location(xMin,yMin,Rectangle(width,height))
  }

  val size: Algebra[ShapeF, Int] = {
    case Rectangle(_, _) => 1
    case Ellipse(_, _) => 1
    case Location(_, _, s) => s
    case Group(shapes @ _*) => shapes.sum
      //How I did Group
      //case Group(shapes@_*) => shapes.foldLeft(0)((a, b)=> a + b)
  }
}


